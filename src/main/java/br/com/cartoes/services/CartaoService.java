package br.com.cartoes.services;

import br.com.cartoes.models.Cartao;
import br.com.cartoes.models.Cliente;
import br.com.cartoes.models.dtos.EntradaCartaoDTO;
import br.com.cartoes.models.dtos.SaidaCartaoDTO;
import br.com.cartoes.models.dtos.SaidaCartaoDTO2;
import br.com.cartoes.models.dtos.StatusCartaoDTO;
import br.com.cartoes.repositories.CartaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sun.rmi.runtime.RuntimeUtil;

import java.util.Optional;

@Service
public class CartaoService {

    @Autowired
    private CartaoRepository cartaoRepository;

    @Autowired
    private ClienteService clienteService;

    public SaidaCartaoDTO criarCartao(EntradaCartaoDTO entradaCartaoDTO) {
        Cliente cliente = clienteService.consultarCliente(entradaCartaoDTO.getClienteId());
        Optional<Cartao> cartaoOptional = cartaoRepository.findByNumero(entradaCartaoDTO.getNumero());
        if(cartaoOptional.isPresent()){
            throw new RuntimeException("Cartão já existe!");
        }
        Cartao cartao = new Cartao();
        cartao.setNumero(entradaCartaoDTO.getNumero());
        cartao.setCliente(cliente);
        cartao.setAtivo(false);
        cartaoRepository.save(cartao);
        SaidaCartaoDTO saidaCartaoDTO = new SaidaCartaoDTO(cartao.getId(), cartao.getNumero(), cliente.getId(), cartao.isAtivo());
        return saidaCartaoDTO;
    }

    public SaidaCartaoDTO2 consultarCartao(int id) {
        Optional<Cartao> cartao = cartaoRepository.findById(id);
        if (cartao.isPresent()) {
            SaidaCartaoDTO2 cartaoDTO = new SaidaCartaoDTO2(cartao.get().getId(), cartao.get().getNumero(),
                    cartao.get().getCliente().getId());
            return cartaoDTO;
        }
        throw new RuntimeException("Cartão não encontrado");
    }

    public SaidaCartaoDTO alteraStatusCartao(int id, StatusCartaoDTO ativo) {
        Optional<Cartao> cartao = cartaoRepository.findById(id);
        if (cartao.isPresent()) {
            cartao.get().setAtivo(ativo.isAtivo());
            Cartao cartao1 = cartaoRepository.save(cartao.get());
            SaidaCartaoDTO saidaCartaoDTO = new SaidaCartaoDTO(cartao1.getId(), cartao1.getNumero(),
                    cartao1.getCliente().getId(), cartao1.isAtivo());
            return saidaCartaoDTO;
        }
        throw new RuntimeException("Cartão não encontrado");
    }
}

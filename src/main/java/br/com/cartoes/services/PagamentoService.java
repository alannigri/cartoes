package br.com.cartoes.services;

import br.com.cartoes.models.Cartao;
import br.com.cartoes.models.Pagamento;
import br.com.cartoes.models.dtos.EntradaPagamentoDTO;
import br.com.cartoes.models.dtos.SaidaPagamentoDTO;
import br.com.cartoes.repositories.CartaoRepository;
import br.com.cartoes.repositories.PagamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class PagamentoService {

    @Autowired
    private PagamentoRepository pagamentoRepository;

    @Autowired
    private CartaoRepository cartaoRepository;

    public SaidaPagamentoDTO realizarPagamento(EntradaPagamentoDTO entradaPagamentoDTO) {
        Optional<Cartao> cartaoOptional = cartaoRepository.findById(entradaPagamentoDTO.getCartao_id());
        if(!cartaoOptional.isPresent()){
            throw new RuntimeException("Cartão não encontrado");
        }
        if(!cartaoOptional.get().isAtivo()){
            throw new RuntimeException("Cartão não está ativo para compras");
        }
        Pagamento pagamento = new Pagamento();
        pagamento.setCartao(cartaoOptional.get());
        pagamento.setDescricao(entradaPagamentoDTO.getDescricao());
        pagamento.setValor(entradaPagamentoDTO.getValor());
        pagamentoRepository.save(pagamento);
        SaidaPagamentoDTO saidaPagamentoDTO = new SaidaPagamentoDTO(pagamento.getId(), pagamento.getCartao().getId(),
                pagamento.getDescricao(), pagamento.getValor());
        return saidaPagamentoDTO;
    }

    public List<SaidaPagamentoDTO> consultarPagamentos(int id_Cartao) {
        Optional<Cartao> cartao = cartaoRepository.findById(id_Cartao);
        if(!cartao.isPresent()){
            throw new RuntimeException("Cartão não encontrado");
        }
        Iterable<Pagamento> pagamentoIterable = pagamentoRepository.findAllByCartao(cartao.get());
        List<SaidaPagamentoDTO> pagamentoDTOList = new ArrayList<>();
        SaidaPagamentoDTO saidaPagamentoDTO;
        for(Pagamento p : pagamentoIterable){
            saidaPagamentoDTO = new SaidaPagamentoDTO(p.getId(), p.getCartao().getId(), p.getDescricao(), p.getValor());
            pagamentoDTOList.add(saidaPagamentoDTO);
        }
        return pagamentoDTOList;
    }
}

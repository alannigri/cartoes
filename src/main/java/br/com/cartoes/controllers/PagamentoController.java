package br.com.cartoes.controllers;

import br.com.cartoes.models.Pagamento;
import br.com.cartoes.models.dtos.EntradaCartaoDTO;
import br.com.cartoes.models.dtos.EntradaPagamentoDTO;
import br.com.cartoes.models.dtos.SaidaPagamentoDTO;
import br.com.cartoes.services.PagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
//@RequestMapping("/pagamentos")
public class PagamentoController {

    @Autowired
    private PagamentoService pagamentoService;

    @PostMapping("/pagamento")
    @ResponseStatus(HttpStatus.CREATED)
    public SaidaPagamentoDTO criarPagamento(@RequestBody EntradaPagamentoDTO pagamento){
    SaidaPagamentoDTO pagamentoSaida = pagamentoService.realizarPagamento(pagamento);
        return pagamentoSaida;
    }

    @GetMapping("/pagamentos/{id_cartao}")
    public List<SaidaPagamentoDTO> consultarPagamentos(@PathVariable int id_cartao){
        List<SaidaPagamentoDTO> saidaPagamentoDTOIterable= pagamentoService.consultarPagamentos(id_cartao);
        return saidaPagamentoDTOIterable;
    }
}

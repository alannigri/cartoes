package br.com.cartoes.controllers;

import br.com.cartoes.models.Cliente;
import br.com.cartoes.services.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/cliente")
public class ClienteController {

    @Autowired
    private ClienteService clienteService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Cliente criarCliente (@RequestBody @Valid Cliente cliente){
        Cliente cliente1 = clienteService.criarCliente(cliente);
        return cliente1;
    }

    @GetMapping("/{id}")
    public Cliente consultarCliente (@PathVariable int id){
        Cliente cliente = clienteService.consultarCliente(id);
        return cliente;
    }

}

package br.com.cartoes.controllers;

import br.com.cartoes.models.Cartao;
import br.com.cartoes.models.dtos.EntradaCartaoDTO;
import br.com.cartoes.models.dtos.SaidaCartaoDTO;
import br.com.cartoes.models.dtos.SaidaCartaoDTO2;
import br.com.cartoes.models.dtos.StatusCartaoDTO;
import br.com.cartoes.services.CartaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/cartao")
public class CartaoController {

    @Autowired
    private CartaoService cartaoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public SaidaCartaoDTO criarCartao (@RequestBody @Valid EntradaCartaoDTO entradaCartaoDTO){
        SaidaCartaoDTO cartao = cartaoService.criarCartao(entradaCartaoDTO);
        return cartao;
    }

    @PatchMapping("/{numero}")
    @ResponseStatus(HttpStatus.OK)
    public SaidaCartaoDTO alteraStatusCartao (@PathVariable int numero, @RequestBody StatusCartaoDTO statusCartaoDTO){
        SaidaCartaoDTO cartao = cartaoService.alteraStatusCartao(numero, statusCartaoDTO);
        return cartao;
    }

    @GetMapping("/{numero}")
    public SaidaCartaoDTO2 consultaCartao (@PathVariable int numero){
        SaidaCartaoDTO2 cartaoDTO = cartaoService.consultarCartao(numero);
        return cartaoDTO;
    }
}

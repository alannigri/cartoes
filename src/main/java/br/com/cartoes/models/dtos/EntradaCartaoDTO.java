package br.com.cartoes.models.dtos;

public class EntradaCartaoDTO {
    private String numero;
    private int clienteId;

    public EntradaCartaoDTO(String numero, int clienteId) {
        this.numero = numero;
        this.clienteId = clienteId;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public int getClienteId() {
        return clienteId;
    }

    public void setClienteId(int clienteId) {
        this.clienteId = clienteId;
    }
}

package br.com.cartoes.models.dtos;

public class StatusCartaoDTO {
    private boolean ativo;

    public StatusCartaoDTO(boolean ativo) {
        this.ativo = ativo;
    }

    public StatusCartaoDTO() {
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }
}
